target remote localhost:1234
file dist/x86_64/prekernel.bin
#file dist/x86_64/k_main.bin

# use the syntax that NASM uses for debugging
set disassembly-flavor intel

# uncomment to enable tui
# tui enable

# break boot32.asm:_start
# break boot32.asm:_begin_init_process
# break boot32.asm:paging_config.loop4

break boot64.asm:_long_mode_start

break k_init.c:k_init

break k_main.c:k_main


# make work...
# zero flag
# set $zf=($eflags >> 6) & 1

# carry flag
# set $cf=$eflags & 1


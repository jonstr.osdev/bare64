FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y \
    build-essential \
    util-linux \
    udev \
    curl \
    parted \
    dosfstools \
    libgmp-dev \
    libmpfr-dev \
    libmpc-dev \
    texinfo \
    libisl-dev \
    wget \
    xz-utils \
    nasm \
    gdb \
    ghex \
    git \
    grub2-common \
    grub-pc-bin \
    lcov \
    nasm \
    qemu-system \
    wmctrl \
    terminator \
    xorriso \
    python3 \
    python3-pip \
    python3-venv \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /opt/cross/src

ENV BINUTILS_VERSION=2.42
ENV GCC_VERSION=13.2.0
ENV GDB_VERSION=14.2

ENV TARGET_32=i686-elf
ENV TARGET_64=x86_64-elf
ENV PREFIX=/opt/cross
ENV PATH="$PREFIX/bin:$PATH"

# Download and extract binutils, GCC, and GDB
RUN wget http://ftp.gnu.org/gnu/binutils/binutils-$BINUTILS_VERSION.tar.gz \
    && tar -xf binutils-$BINUTILS_VERSION.tar.gz \
    && wget http://ftp.gnu.org/gnu/gcc/gcc-$GCC_VERSION/gcc-$GCC_VERSION.tar.gz \
    && tar -xf gcc-$GCC_VERSION.tar.gz \
    && wget https://ftp.gnu.org/gnu/gdb/gdb-$GDB_VERSION.tar.xz \
    && tar -xf gdb-$GDB_VERSION.tar.xz

# Build and install binutils for both i686-elf and x86_64-elf
RUN mkdir -p build-binutils-i686 \
    && cd build-binutils-i686 \
    && ../binutils-$BINUTILS_VERSION/configure --target=$TARGET_32 --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror \
    && make -j$(nproc) \
    && make install \
    && cd .. \
    && mkdir -p build-binutils-x86_64 \
    && cd build-binutils-x86_64 \
    && ../binutils-$BINUTILS_VERSION/configure --target=$TARGET_64 --prefix="$PREFIX" --with-sysroot --disable-nls --disable-werror \
    && make -j$(nproc) \
    && make install \
    && cd ..

# Build and install GCC for i686-elf
RUN mkdir -p build-gcc-i686 \
    && cd build-gcc-i686 \
    && ../gcc-$GCC_VERSION/configure --target=$TARGET_32 --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers \
    && make all-gcc -j$(nproc) \
    && make all-target-libgcc -j$(nproc) \
    && make install-gcc \
    && make install-target-libgcc \
    && cd ..

# Build and install for x86_64-elf GCC build and install (red zone)
RUN mkdir -p build-gcc-x86_64 \
    && cd build-gcc-x86_64 \
    && ../gcc-$GCC_VERSION/configure --target=$TARGET_64 --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers \
    && make all-gcc -j$(nproc) \
    && make all-target-libgcc -j$(nproc) \
    && make install-gcc \
    && make install-target-libgcc \
    && cd ..

# Prepare GCC sources, no-red-zone configuration for x86_64
RUN cd /opt/cross/src/gcc-$GCC_VERSION \
    && echo "MULTILIB_OPTIONS += mno-red-zone" >> gcc/config/i386/t-x86_64-elf \
    && echo "MULTILIB_DIRNAMES += no-red-zone" >> gcc/config/i386/t-x86_64-elf \
    && sed -i '/x86_64-\*-elf\*)/a tmake_file="${tmake_file} i386/t-x86_64-elf"' gcc/config.gcc \
    && cd ..

# Build and install GCC for x86_64-elf (no red zone)
RUN cd /opt/cross/src/build-gcc-x86_64 \
    && ../gcc-$GCC_VERSION/configure --target=$TARGET_64 --prefix="$PREFIX" --disable-nls --enable-languages=c,c++ --without-headers \
    && make all-gcc -j$(nproc) \
    && make all-target-libgcc -j$(nproc) \
    && make install-gcc \
    && make install-target-libgcc \
    && cd ..

# Build and install GDB for i686-elf
RUN mkdir -p build-gdb-i686 \
    && cd build-gdb-i686 \
    && ../gdb-$GDB_VERSION/configure --target=$TARGET_32 --prefix="$PREFIX" --disable-werror \
    && make -j$(nproc) all-gdb \
    && make install-gdb \
    && cd ..

# Build and install GDB for x86_64-elf
RUN mkdir -p build-gdb-x86_64 \
    && cd build-gdb-x86_64 \
    && ../gdb-$GDB_VERSION/configure --target=$TARGET_64 --prefix="$PREFIX" --disable-werror \
    && make -j$(nproc) all-gdb \
    && make install-gdb \
    && cd ..

# Clean up
RUN rm -rf /opt/cross/src

# Set path
ENV PATH="/opt/cross/bin:${PATH}"

WORKDIR /bare64
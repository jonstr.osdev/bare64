
# Directories
SRC_DIR := src
BUILD_DIR := build
DIST_DIR := dist
ASM_DIR := $(SRC_DIR)/impl/x86_64
C_DIR := $(SRC_DIR)/impl/x86_64
KERNEL_C_DIR := $(SRC_DIR)/impl/prekernel
ISO_DIR := targets/x86_64/iso
HARDDRIVE = disk/mydisk.img
DISK_SIZE = 4G
MNT_SANDBOX = /mnt/bare64-disk
#TERM = terminator

# Toolchain
AS := nasm
CC := x86_64-elf-gcc
LD := x86_64-elf-ld
GRUB_MKRESCUE := grub-mkrescue

# QEMU and GDB
QEMU := qemu-system-x86_64
GDB := gdb # Or just 'gdb' depending on your setup

# QEMU with GDB debugging
QEMUFLAGS := -cdrom $(ISO)
QEMU_DEBUG_FLAGS := -s -S

# Flags
ASMFLAGS := -g -F dwarf -f elf64
CFLAGS := -I$(SRC_DIR)/intf -Og -ffreestanding -ffreestanding -mcmodel=large -mno-red-zone -mno-mmx -mno-sse -mno-sse2 -ggdb
LDFLAGS := -n -T targets/x86_64/linker.ld

# Source files
ASM_SRC := $(shell find $(ASM_DIR) -name '*.asm')
C_SRC := $(shell find $(C_DIR) $(KERNEL_C_DIR) -name '*.c')

# Object files
OBJ := $(ASM_SRC:$(ASM_DIR)/%.asm=$(BUILD_DIR)/%.o) $(C_SRC:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)

# Dependency files
DEP := $(OBJ:.o=.d)

# Target
TARGET := $(DIST_DIR)/x86_64/prekernel.bin
ISO := $(DIST_DIR)/x86_64/prekernel.iso


# Rules
all: $(ISO)


$(ISO): $(TARGET)
	@mkdir -p $(ISO_DIR)/boot
	cp $(TARGET) $(ISO_DIR)/boot/prekernel.bin
	$(GRUB_MKRESCUE) /usr/lib/grub/i386-pc -o $(ISO) $(ISO_DIR)


$(TARGET): $(OBJ)
	@mkdir -p $(@D)
	$(LD) $(LDFLAGS) -o $@ $^


# Assembly source
$(BUILD_DIR)/%.o: $(ASM_DIR)/%.asm
	@mkdir -p $(@D)
	$(AS) $(ASMFLAGS) $< -o $@


# C source
$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $< -o $@


# Include dependency files
-include $(DEP)


.PHONY: clean docker_build docker_shell run debug create_disk setup_grub delete_disk wipe_disk run_disk debug_disk

docker_build:
	docker build -t xcompilers:latest . 2>&1 | tee build.log

docker_shell:
	xhost +local:docker	
	docker run --privileged -v $(CURDIR):/bare64 -v /dev:/dev -v /tmp/.X11-unix:/tmp/.X11-unix -e DISPLAY=$$DISPLAY -it xcompilers:latest /bin/bash


# Run without debugging
run: $(ISO)
	$(QEMU) -cdrom $(ISO) -m 1G


# Debug configuration
debug: $(TARGET)
	touch dbg_serial.out
	$(QEMU) -cdrom $(ISO) -debugcon file:dbg_serial.out -s -S &
	sleep 1 # Give QEMU a moment to start
	$(GDB) -x .gdbinit

	disk: create_disk setup_grub

 
disk: $(TARGET)
	@chmod +x scripts/make_k_main.sh
	@./scripts/make_k_main.sh

	@chmod +x scripts/create_disk.sh
	@./scripts/create_disk.sh $(HARDDRIVE) $(DISK_SIZE)

	@chmod +x scripts/setup_grub.sh
	@./scripts/setup_grub.sh $(HARDDRIVE) $(MNT_SANDBOX) prekernel.bin $(TARGET)


setup_grub:
	@chmod +x scripts/setup_grub.sh
	@./scripts/setup_grub.sh $(HARDDRIVE) $(MNT_SANDBOX) prekernel.bin $(TARGET)
	

delete_disk:
	@echo "Deleting disk image at '$(HARDDRIVE)' ..."
	@test -f $(HARDDRIVE) && rm $(HARDDRIVE) || echo "No disk image to delete."


wipe_disk: delete_disk create_disk setup_grub


run_disk:
	$(QEMU) -drive format=raw,file=$(HARDDRIVE),index=0,media=disk


debug_disk: 
	touch dbg_serial.out
	$(QEMU) -debugcon file:dbg_serial.out -drive format=raw,file=$(HARDDRIVE),index=0,media=disk -s -S &
	sleep 1
	$(GDB) -x .gdbinit
	sleep 1

# Clean target should now also clean k_main
clean: delete_disk
	@./scripts/make_k_main.sh clean
	rm -rf $(BUILD_DIR) $(DIST_DIR)
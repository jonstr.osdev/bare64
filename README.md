# Project Setup and Execution Guide

## Overview

This guide provides detailed instructions for setting up the environment, building, and running the project. It focuses on the importance of a cross-compiler with configurations for both red zone and no red zone support. Unlike previous versions, which required the user to build the toolchain from scratch, this version simplifies the process with a Dockerfile that automates the toolchain construction and installation.

## Prerequisites

Before starting, ensure the following dependencies are installed:

- `Linux`
- `docker`
- `make`

Additionally, verify that there is at least 4GB of free disk space available to create the disk image.

## Building the Project

To construct the Docker container, execute:

```bash
make docker_build
```
Once the build is complete, you can access the container with:

```bash
make docker_shell
```

Inside the container, create the necessary disk image for the project:

```bash
make disk
```

## Running the Project
The project can be executed in various modes, tailored to your requirements. Start by initializing an xhost instance outside the container to allow the container to interface with it:

```bash
xhost +local:docker
```

## Normal Execution
For standard project execution inside the Docker container:

```bash
make run_disk
```

## Debug Mode
To run the project in debug mode within the Docker container:

```bash
make debug_disk
```
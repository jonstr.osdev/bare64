#!/bin/bash

# ripped from jonstr.osdev/bootable-barebones.git

#########################################################################################
# - WARNING -
#    - Listen, you may have seen people say "NO IMPLIED WARRANTY," etc...
#		If you run this script improperly, it WILL break your machine!
#		So please be careful and read the script before running it.
#       under no circumstance should you use any features of this script
#       outside of a virtual machine or a sandbox environment.
#    - I am not responsible for any damage to your machine or data.
#	 - You have been warned.
#
#########################################################################################

HARDDRIVE=$1
DISK_SIZE=$2

echo "Creating Disk Image ..."
echo "You are about to create a new disk image at '$HARDDRIVE' with size $DISK_SIZE."
read -p "Are you sure you want to proceed with creating the disk image at '$HARDDRIVE'? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1

qemu-img create -f raw $HARDDRIVE $DISK_SIZE &&
    echo "Disk image '$HARDDRIVE' was created successfully." || (
    echo "Failed to create disk image at '$HARDDRIVE'."
    exit 1
)

read -p "This will create a MS-DOS partition table and a primary FAT32 partition on '$HARDDRIVE'. Are you sure you want to proceed? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1

parted $HARDDRIVE --script mklabel msdos &&
    parted $HARDDRIVE --script mkpart primary fat32 1MiB 100% &&
    parted $HARDDRIVE --script set 1 boot on &&
    echo "Partition table and FAT partition created and configured on '$HARDDRIVE'." || (
    echo "Failed to create and configure the partition table and FAT partition on $HARDDRIVE."
    exit 1
)

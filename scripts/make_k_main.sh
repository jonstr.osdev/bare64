#!/bin/bash

SCRIPT_DIR="$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"
TARGET_DIR="$SCRIPT_DIR/../src/impl/kernel"

cd "$TARGET_DIR" || exit 1

if [ "$1" = "clean" ]; then
    echo "Cleaning k_main.bin ..."
    make clean
else
    echo "Building k_main.bin ..."
    make clean
    make
fi

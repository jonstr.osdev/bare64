#!/bin/bash

#########################################################################################
# - WARNING -
#    - Listen, you may have seen people say "NO IMPLIED WARRANTY," etc...
#		If you run this script improperly, it WILL break your machine!
#		So please be careful and read the script before running it.
#       under no circumstance should you use any features of this script
#       outside of a virtual machine or a sandbox environment.
#    - I am not responsible for any damage to your machine or data.
#	 - You have been warned.
#
#########################################################################################

HARDDRIVE=$1
MNT_SANDBOX=$2
LOOPDEV=$(losetup -fP --show "$HARDDRIVE")
BINNAME=$3
TARGET=$4

if [ -z "$LOOPDEV" ]; then
    echo "Failed to set up loop device."
    exit 1
fi

echo "Using loop device '$LOOPDEV'."
read -p "You are about to format '"$LOOPDEV"p1' as FAT32. Continue? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1
mkfs.fat -F 32 "$LOOPDEV"p1 &&
    echo "Filesystem on '"$LOOPDEV"p1' created." || (
    echo "Failed to create filesystem on '"$LOOPDEV"p1'."
    exit 1
)

read -p "Now mounting '"$LOOPDEV"p1' to '$MNT_SANDBOX'. Continue? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1
mkdir -p $MNT_SANDBOX
mount "$LOOPDEV"p1 $MNT_SANDBOX &&
    echo "Mounted '$LOOPDEV'p1 to '$MNT_SANDBOX'." || (
    echo "Failed to mount '$LOOPDEV'p1 to '$MNT_SANDBOX'."
    exit 1
)

read -p "Proceed to install GRUB (i386-pc) on '$LOOPDEV'? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1
grub-install --target=i386-pc --root-directory=$MNT_SANDBOX --no-floppy --modules="normal part_msdos ext2 multiboot" "$LOOPDEV" &&
    echo "GRUB (i386-pc) installed on '$LOOPDEV'." || (
    echo "GRUB (i386-pc) installation on '$LOOPDEV' failed."
    exit 1
)

read -p "Copy kernel binaries to the boot directory on '"$LOOPDEV"p1'? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1

cp $TARGET "$MNT_SANDBOX/boot/$BINNAME" &&
    echo "'$BINNAME' copied to '$MNT_SANDBOX/boot'." || (
    echo "Failed to copy '$BINNAME' to '$MNT_SANDBOX/boot'."
    exit 1
)

cp targets/x86_64/iso/boot/k_main.bin "$MNT_SANDBOX/boot/k_main.bin" &&
    echo "'k_main.bin' copied to '$MNT_SANDBOX/boot'." || (
    echo "Failed to copy 'k_main.bin' to '$MNT_SANDBOX/boot'."
    exit 1
)

read -p "Copy GRUB configuration file to disk? [y/N]: " confirm
cp targets/x86_64/iso/boot/grub/grub.cfg "$MNT_SANDBOX/boot/grub/grub.cfg" &&
    echo "'targets/x86_64/iso/boot/grub/grub.cfg' copied to '$MNT_SANDBOX/boot'." || (
    echo "Failed to copy 'targets/x86_64/iso/boot/grub/grub.cfg' to '$MNT_SANDBOX/boot'."
    exit 1
)

read -p "Final step: Unmount '$MNT_SANDBOX' and detach loop device '$LOOPDEV'. Proceed? [y/N]: " confirm
[[ $confirm == [yY] ]] || exit 1
umount $MNT_SANDBOX &&
    echo "Unmounted '$MNT_SANDBOX'." || (
    echo "Failed to unmount '$MNT_SANDBOX'."
    exit 1
)
losetup -d "$LOOPDEV" &&
    echo "Detached loop device '$LOOPDEV'." || (
    echo "Failed to detach loop device '$LOOPDEV'."
    exit 1
)

echo "GRUB setup on '$HARDDRIVE' complete."

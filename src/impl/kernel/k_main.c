
#define VID_MEM_ADDRESS 0xB8000

//this method puts a char at 0xB8000, takes a char and a color, row as an offset and coloumn as an offset
void k_putc(const char c, unsigned char color, unsigned int row, unsigned int col)
{
    unsigned short *vid_mem = (unsigned short *)VID_MEM_ADDRESS;
    unsigned int offset = (row * 80) + col;
    vid_mem[offset] = (color << 8) | c;
}

//this method clears the screen
void k_cls()
{
    for (int i = 0; i < 80; i++)
    {
        for (int j = 0; j < 25; j++)
        {
            k_putc(' ', 0x0F, j, i);
        }
    }
}

void k_puts(const char *str, unsigned char color, unsigned int row, unsigned int col)
{
    unsigned short *vid_mem = (unsigned short *)VID_MEM_ADDRESS;
    unsigned int offset = (row * 80) + col;
    for (int i = 0; str[i] != '\0'; i++)
    {
        vid_mem[offset + i] = (color << 8) | str[i];
    }
}



extern void k_main(void)
{
    k_cls();
    k_puts("Hello World, THIS IS KERNEL MODULE CODE!", 0x0F, 0, 0);
}
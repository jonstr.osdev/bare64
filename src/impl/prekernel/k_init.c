#include "../../intf/screen.h"
#include "../../intf/multiboot2.h"

#include <stddef.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdbool.h>

static void halt()
{
    asm volatile("hlt");
    __builtin_unreachable();
}

void assert_failed(const char *file, const char *expression)
{
    screen_puts("Assertion failed: ");
    screen_puts(expression);
    screen_puts(" in ");
    screen_puts(file);
    screen_puts("\n");

    halt();

    while (1)
    {
    } // Infinite loop as a fallback
}

#define assert(expression) \
    ((expression) ? (void)0 : assert_failed(__FILE__, #expression))

#define VERIFY(expression) assert(expression)
#define array_size(arr) (sizeof(arr) / sizeof((arr)[0]))

void *memcpy(void *dest, const void *src, size_t n)
{
    char *d = dest;
    const char *s = src;
    while (n--)
    {
        *d++ = *s++;
    }
    return dest;
}

void *memset(void *s, int c, size_t n)
{
    unsigned char *p = s;
    while (n--)
    {
        *p++ = (unsigned char)c;
    }
    return s;
}

char *strstr(const char *haystack, const char *needle)
{
    if (!*needle)
    {
        return (char *)haystack;
    }
    for (; *haystack; ++haystack)
    {
        if (*haystack == *needle)
        {
            const char *h, *n;
            for (h = haystack, n = needle; *h && *n; ++h, ++n)
            {
                if (*h != *n)
                {
                    break;
                }
            }
            if (!*n)
            {
                return (char *)haystack;
            }
        }
    }
    return 0;
}

typedef struct
{
    uint32_t start_of_prekernel_image;
    uint32_t end_of_prekernel_image;
    uint64_t physical_to_virtual_offset;
    uint64_t kernel_mapping_base;
    uint64_t kernel_load_base;
    uint32_t gdt64ptr;
    uint16_t code64_sel;
    uint32_t boot_pml4t;
    uint32_t boot_pdpt;
    uint32_t boot_pd0;
    uint32_t boot_pd_kernel;
    uint64_t boot_pd_kernel_pt1023;
    uint64_t kernel_cmdline;
    uint32_t multiboot_flags;
    uint64_t multiboot_memory_map;
    uint32_t multiboot_memory_map_count;
    uint64_t multiboot_modules;
    uint32_t multiboot_modules_count;
    uint64_t multiboot_framebuffer_addr;
    uint32_t multiboot_framebuffer_pitch;
    uint32_t multiboot_framebuffer_width;
    uint32_t multiboot_framebuffer_height;
    uint8_t multiboot_framebuffer_bpp;
    uint8_t multiboot_framebuffer_type;
} boot_info_t;

static void k_vmemmove(void *dest_virt, uintptr_t dest_phys, void *src, size_t n)
{
    if (dest_phys < (uintptr_t)src)
    {
        uint8_t *pd = (uint8_t *)dest_virt;
        uint8_t const *ps = (uint8_t const *)src;

        for (; n--;)
        {
            *pd++ = *ps++;
        }

        return;
    }

    uint8_t *pd = (uint8_t *)dest_virt;
    uint8_t const *ps = (uint8_t const *)src;

    for (pd += n, ps += n; n--;)
    {
        *--pd = *--ps;
    }
}

#define EI_NIDENT 16 /* Gfx::Size of e_ident[] */

// Simplified ELF header and program header structures.
typedef struct
{
    unsigned char e_ident[16]; // Magic number and other info
    uint16_t e_type;           // Object file type
    uint16_t e_machine;        // Architecture
    uint32_t e_version;        // Object file version
    uint64_t e_entry;          // Entry point virtual address
    uint64_t e_phoff;          // Program header table file offset
    uint64_t e_shoff;          // Section header table file offset
    uint32_t e_flags;          // Processor-specific flags
    uint16_t e_ehsize;         // ELF header size in bytes
    uint16_t e_phentsize;      // Program header table entry size
    uint16_t e_phnum;          // Program header table entry count
    uint16_t e_shentsize;      // Section header table entry size
    uint16_t e_shnum;          // Section header table entry count
    uint16_t e_shstrndx;       // Section header string table index
} Elf64_Ehdr;

typedef struct
{
    uint32_t p_type;   // Segment type
    uint32_t p_flags;  // Segment flags
    uint64_t p_offset; // Segment file offset
    uint64_t p_vaddr;  // Segment virtual address
    uint64_t p_paddr;  // Segment physical address
    uint64_t p_filesz; // Segment size in file
    uint64_t p_memsz;  // Segment size in memory
    uint64_t p_align;  // Segment alignment
} Elf64_Phdr;

#define PT_LOAD 1

/* Segment types - p_type */
#define PT_NULL 0          /* unused */
#define PT_LOAD 1          /* loadable segment */
#define PT_DYNAMIC 2       /* dynamic linking section */
#define PT_INTERP 3        /* the RTLD */
#define PT_NOTE 4          /* auxiliary information */
#define PT_SHLIB 5         /* reserved - purpose undefined */
#define PT_PHDR 6          /* program header */
#define PT_TLS 7           /* thread local storage */
#define PT_LOOS 0x60000000 /* reserved range for OS */
#define PT_HIOS 0x6fffffff /* specific segment types */

#ifndef PAGE_SIZE
#define PAGE_SIZE 4096
#endif

// Struct definition
typedef struct
{
    uint32_t m_eax;
    uint32_t m_ebx;
    uint32_t m_ecx;
    uint32_t m_edx;
} CPUID;

// CPUID function declarations
void CPUID_init(CPUID *cpuid, uint32_t function, uint32_t ecx);
uint32_t CPUID_eax(const CPUID *cpuid);
uint32_t CPUID_ebx(const CPUID *cpuid);
uint32_t CPUID_ecx(const CPUID *cpuid);
uint32_t CPUID_edx(const CPUID *cpuid);

// Inline assembly functions declarations
static inline uint64_t read_tsc();
static inline uint32_t read_rdrand();
static inline uint32_t read_rdseed();

// Ensure CPUID_init and the CPUID access functions are defined before generate_secure_seed
void CPUID_init(CPUID *cpuid, uint32_t function, uint32_t ecx)
{
    cpuid->m_eax = 0xffffffff;
    cpuid->m_ebx = 0xffffffff;
    cpuid->m_ecx = 0xffffffff;
    cpuid->m_edx = 0xffffffff;

    asm volatile("cpuid"
                 : "=a"(cpuid->m_eax), "=b"(cpuid->m_ebx), "=c"(cpuid->m_ecx), "=d"(cpuid->m_edx)
                 : "a"(function), "c"(ecx));
}

uint32_t CPUID_eax(const CPUID *cpuid) { return cpuid->m_eax; }
uint32_t CPUID_ebx(const CPUID *cpuid) { return cpuid->m_ebx; }
uint32_t CPUID_ecx(const CPUID *cpuid) { return cpuid->m_ecx; }
uint32_t CPUID_edx(const CPUID *cpuid) { return cpuid->m_edx; }

// Inline assembly function definitions
static inline uint64_t read_tsc()
{
    uint32_t lo, hi;
    asm volatile("rdtsc" : "=a"(lo), "=d"(hi));
    return ((uint64_t)hi << 32) | lo;
}

static inline uint32_t read_rdrand()
{
    uint32_t val;
    asm volatile("1: rdrand %0; jnc 1b" : "=r"(val));
    return val;
}

static inline uint32_t read_rdseed()
{
    uint32_t val;
    asm volatile("1: rdseed %0; jnc 1b" : "=r"(val));
    return val;
}

uint64_t generate_secure_seed()
{
    uint32_t seed = 0xFEEBDAED;
    CPUID processor_info;
    CPUID extended_features;

    // Initialize CPUID with 0x1 to get processor info
    CPUID_init(&processor_info, 0x1, 0);

    // Check for TSC availability and XOR with TSC if available
    if (CPUID_edx(&processor_info) & (1 << 4)) // TSC
    {
        seed ^= read_tsc();
    }

    // Check for RDRAND availability and XOR with RDRAND if available
    if (CPUID_ecx(&processor_info) & (1 << 30)) // RDRAND
    {
        seed ^= read_rdrand();
    }

    // Initialize CPUID with 0x7 to get extended features
    CPUID_init(&extended_features, 0x7, 0);

    // Check for RDSEED availability and XOR with RDSEED if available
    if (CPUID_ebx(&extended_features) & (1 << 18)) // RDSEED
    {
        seed ^= read_rdseed();
    }

    // Incorporate multiboot information into the seed
    // seed ^= multiboot_info_ptr->mods_addr;
    // seed ^= multiboot_info_ptr->framebuffer_addr;

    return seed;
}

bool load_and_execute_module(uint64_t mod_start, uint64_t mod_end)
{
    Elf64_Ehdr *header = (Elf64_Ehdr *)mod_start;

    if ((uint64_t)header + sizeof(Elf64_Ehdr) > mod_end)
    {
        screen_puts("Invalid or truncated ELF header.\n");
        return false;
    }

    // Variable to store the adjusted entry point
    uint64_t adjusted_entry = 0;

    for (uint16_t i = 0; i < header->e_phnum; i++)
    {
        Elf64_Phdr *phdr = (Elf64_Phdr *)(mod_start + header->e_phoff + (i * header->e_phentsize));

        if ((uint64_t)phdr + sizeof(Elf64_Phdr) > mod_end)
        {
            screen_puts("Invalid or truncated program header.\n");
            return false;
        }

        if (phdr->p_type == PT_LOAD)
        {
            // Assume p_vaddr is the intended virtual address
            uint64_t segment_physical_start = mod_start + phdr->p_offset;
            uint64_t segment_physical_end = segment_physical_start + phdr->p_filesz;

            if (segment_physical_end > mod_end)
            {
                screen_puts("Segment exceeds module bounds.\n");
                return false;
            }

            // For the segment containing the entry point, adjust the entry point to the physical address space
            if (header->e_entry >= phdr->p_vaddr && header->e_entry < (phdr->p_vaddr + phdr->p_memsz))
            {
                adjusted_entry = segment_physical_start + (header->e_entry - phdr->p_vaddr);
            }

            memcpy((void *)(phdr->p_vaddr), (void *)segment_physical_start, phdr->p_filesz);
            if (phdr->p_memsz > phdr->p_filesz)
            {
                memset((void *)(phdr->p_vaddr + phdr->p_filesz), 0, phdr->p_memsz - phdr->p_filesz);
            }
        }
    }

    // Check if the entry point was successfully adjustedcall t
    if (adjusted_entry == 0)
    {
        screen_puts("Invalid entry point.\n");
        return false;
    }

    // Jump to the adjusted entry point
    void (*entry)(void) = (void (*)(void))adjusted_entry;
    entry();

    return true;
}

void k_init(uint64_t magic, uint64_t mb2i_addr)
{
    screen_cls();
    screen_set_color(SCREEN_COLOR_YELLOW, SCREEN_COLOR_BLACK);
    screen_puts("Hello, World in 64 bits!\n");

    // Align the address
    mb2i_addr = (mb2i_addr + 7) & ~7;

    // Cast the address to a Multiboot2 info structure
    struct multiboot_tag *tag = (struct multiboot_tag *)(mb2i_addr + 8);

    // Loop through all tags until you find the module tag
    while (tag->type != MULTIBOOT_TAG_TYPE_END)
    {
        if (tag->type == MULTIBOOT_TAG_TYPE_MODULE)
        {
            struct multiboot_tag_module *mod_tag = (struct multiboot_tag_module *)tag;
            if (!load_and_execute_module(mod_tag->mod_start, mod_tag->mod_end))
            {
                screen_puts("Failed to load and execute module.\n");
                halt();
            }
            break;
        }
        tag = (struct multiboot_tag *)((uint8_t *)tag + ((tag->size + 7) & ~7));
    }

    halt();
}
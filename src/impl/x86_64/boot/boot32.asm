;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; JON-STR 03-05-2024
;;;     - 64 bit kernel
;;;         - boilerplate
;;;     - entry point
;;; NOTE: Optimize this by removing pushes and pops 
;;;        of ebx when not necessary
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
%if 0

THIS IS A COMMENT BLOCK IN NASM. IT IS SLOPPY AND GROSS.
IT WANTS TO BE FRIENDS WITH YOU BUT IT IS TOO SHY TO ASK.

%endif
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; CONSTANTS ;;;
%define CHAR_CR             0x0D
%define CHAR_LF             0x0A

TEXT_MODE_VID_MEM_ADDR  equ 0xB8000
MULTIBOOT_MAGIC         equ 0x36D76289

CPU_ID_BIT              equ 21
CPU_LM_BIT              equ 29 ; long mode bit

PAE_BIT                 equ 5

ENABLE_LM_MAGIC         equ 0xC0000080

ENABLE_LM_BIT           equ 8
ENABLE_PAGING_BIT       equ 31

EXEC_FLAG_BIT           equ 43
DESCRIPTOR_TYPE_BIT     equ 44
PRESENT_FLAG_BIT        equ 47
X86_64_FLAG_BIT         equ 53

DEBUG_SERIAL_PORT       equ 0xE9

MAX_KERNEL_SIZE         equ 0x4000000
KERNEL_PD_SIZE          equ 0x31000000

;;; GLOBALS ;;;
global _start
extern _long_mode_start
extern k_init
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; MACROS;;;
%macro MAKE_DEBUG_STRING 1
    db %1, CHAR_CR, CHAR_LF, 0
%endmacro
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
section .bss
    align 4096

global kernel_cmdline
    kernel_cmdline:
        resb 4096

section .page_tables nobits
    align 4096

    global boot_pml4t
    boot_pml4t:
        resb 4096

    global boot_pdpt
    boot_pdpt:
        resb 4096

    global boot_pd0
    boot_pd0:
        resb 4096 * 4

    global boot_pd0_pts
    boot_pd0_pts:
        resb 4096 * (MAX_KERNEL_SIZE >> 21)

    global boot_pd_kernel
    boot_pd_kernel:
        resb 4096

    global boot_pd_kernel_pt0
    boot_pd_kernel_pt0:
        resb 4096 

    global boot_pd_kernel_image_pts
    boot_pd_kernel_image_pts:
        resb 4096 * (KERNEL_PD_SIZE >> 21)

    global boot_pd_kernel_pt1023
    boot_pd_kernel_pt1023:
        resb 4096


section .stack nobits 
    align 4096
    stack_bottom:
        resb 4096 * 8
    stack_top:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
section .rodata

; Access bits
PRESENT        equ 1 << 7
NOT_SYS        equ 1 << 4
EXEC           equ 1 << 3
DC             equ 1 << 2
RW             equ 1 << 1
ACCESSED       equ 1 << 0
 
; Flags bits
GRAN_4K       equ 1 << 7
SZ_32         equ 1 << 6
LONG_MODE     equ 1 << 5

global gdt64ptr

gdt64: 
.null:
    dq 0

.code_seg: equ $ - gdt64 
    ; dq ((1 << EXEC_FLAG_BIT) | (1 << DESCRIPTOR_TYPE_BIT) | (1 << PRESENT_FLAG_BIT) | (1 << X86_64_FLAG_BIT)) 
    dd 0xFFFF
    db 0
    db PRESENT | NOT_SYS | EXEC | RW
    db GRAN_4K | LONG_MODE | 0xF
    db 0
.data_seg: equ $ - gdt64
    dq 0xFFFF
    db 0
    db PRESENT | NOT_SYS | RW
    db GRAN_4K | SZ_32 | 0xF
    db 0
.tss: equ $ - gdt64
    dd 0x00000068
    dd 0x00CF8900
.pointer:
gdt64ptr:
    dw $ - gdt64 - 1 ; gdt size - 1
    dq gdt64

code64_sel_value equ gdt64.code_seg - gdt64

global code64_sel
code64_sel: dw code64_sel_value
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 1st STAGE ENTRY POINT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
section .text

bits 32

_init_dbg_msg: MAKE_DEBUG_STRING "BARE64: Initializing ..."
_halting_dbg_msg: MAKE_DEBUG_STRING "BARE64: Halting ..." 

_start:
    ; consider using bootstack if this preloader gets much bigger
    mov esp, stack_top

    push _init_dbg_msg
    call print_dbg_serial

    ; no ret addr on stack
    jmp _begin_init_process

.halting:
    push _halting_dbg_msg
    call print_dbg_serial

    hlt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 2nd STAGE ENTRY POINT
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
extern end_of_prekernel_image

_load_failure_dbg_msg: MAKE_DEBUG_STRING "ERR: Kernel is too large"

_begin_init_process:
    cli
    cld
    mov esi, end_of_prekernel_image
    cmp esi, MAX_KERNEL_SIZE
    
    jbe .load_kernel

.load_failure:
    push _load_failure_dbg_msg
    call print_dbg_serial

    ret

.load_kernel:
    push eax
    push ebx
    push ecx
    push edx

    ; determine compatibility
    call check_multiboot
    call check_cpuid
    call check_pae
    call check_long_mode

    pop edx
    pop ecx
    pop ebx
    pop eax

    ; set up page tables and enable paging if compatible
    jmp paging_config ; serenityos paging via OSDev wiki

.infinity:
    hlt
    jmp .infinity

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;func:
;  print_no_halt: prints a C string to the screen
;  - param 1: pointer to C string (32 bit addr [ebp + 8])
;  - return: length of string at esi (zero terminated)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print_no_halt:
    push ebp
    mov ebp, esp

    push esi
    push ecx
    push eax

    mov esi, dword [ebp + 8]

    mov ecx, TEXT_MODE_VID_MEM_ADDR     ; VRAM address.
    mov ax, 0x07                        ; grey-on-black text.
    shl ax, 8                           ; shift it into the high byte.         

.print_str_loop:
    lodsb                               ; Loads a byte from address at %esi into %al and increments %esi. 

    test al, al
    jz .print_str_end

    mov [ecx], ax                       ; Store the character and its attribute in VRAM.
    add ecx, 2                          ; Move to the next character cell in VRAM, 2 bytes per character cell.

    jmp .print_str_loop

.print_str_end:
    pop eax
    pop ecx
    pop esi

    mov esp, ebp
    pop ebp

    pop dword [esp]                     ; remove dbg msg from stack

    ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; func:
;   print_dbg_serial: prints a C string to the screen
;   - param 1: pointer to C string (32 bit addr [ebp + 8])
;   - return: length of string at esi (zero terminated)
;   
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
print_dbg_serial:
    push ebp
    mov ebp, esp

    push esi
    push ecx
    push eax
    push edx                    ; JIC, not really necessary

    mov esi, dword [ebp + 8]    ; here we have the string

    mov dx, DEBUG_SERIAL_PORT

.print_str_loop:
    lodsb                       ; Loads a byte from address at %esi into %al and increments %esi. 

    test al, al                 ; check if we're at the end of the string
    jz .print_str_end

    out dx, al                  ; write character to serial port

    jmp .print_str_loop

.print_str_end:
    pop edx
    pop eax
    pop ecx
    pop esi

    mov esp, ebp
    pop ebp

    pop dword [esp]             ; remove dbg msg from stack

    ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_pae_unsupported_dbg_msg: MAKE_DEBUG_STRING "ERR: PAE is not supported"

check_pae:
    push eax
    push ebx    ; JIC, not necessary
    push edx

    mov eax, 0x01
    cpuid
    test edx, 1 << 6
    jnz .pae_supported

.pae_unsupported:
    push _pae_unsupported_dbg_msg
    call print_dbg_serial
    ret

.pae_supported:
    pop edx
    pop ebx
    pop eax
    ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_multiboot_ubsupported_dbg_msg: MAKE_DEBUG_STRING "ERR: Multiboot is not supported"

check_multiboot:
    cmp eax, MULTIBOOT_MAGIC
    jne .multiboot_unsupported
    ret

.multiboot_unsupported:
    push _multiboot_ubsupported_dbg_msg
    call print_dbg_serial
    hlt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_cpuid_unsupported_dbg_msg: MAKE_DEBUG_STRING "ERR: CPUID is not supported"

check_cpuid:
    push eax
    push ebx    ; JIC, not necessary

    pushfd

    pop eax

    mov ecx, eax
    xor eax, 1 << CPU_ID_BIT
    push eax

    popfd
    pushfd

    pop eax
    push ecx

    popfd

    cmp eax, ecx
    je .cpuid_unsupported

    pop ebx
    pop eax

    ret

.cpuid_unsupported:
    push _cpuid_unsupported_dbg_msg
    call print_dbg_serial
    hlt
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_check_long_mode_dbg_msg: MAKE_DEBUG_STRING "ERR: Long Mode is Not Supported!!!"

check_long_mode:
    push eax
    push ebx    ; JIC, not necessary

    mov eax, 0x80000000
    cpuid
    test edx, (1 << 29)
    jnz .long_mode_supported

.long_mode_unsupported:
    push _check_long_mode_dbg_msg
    call print_dbg_serial
    ret

.long_mode_supported:
    pop ebx
    pop eax
    ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
paging_config:
    ; preserve mb2 magic and mb2 info ptr
    push eax
    push ebx

    ; move commandline to a known location
    mov esi, ebx
    add esi, 16
    mov esi, [esi]
    mov ecx, 1024
    mov edi, kernel_cmdline
    rep movsd

    ; set up the page tables

    ; clear pml4t
    mov edi, boot_pml4t
    mov ecx, 1024
    xor eax, eax
    rep stosd

    ; Set up pml4t[0]
    mov edi, boot_pml4t
    mov dword [edi], boot_pdpt
    or dword [edi], 0x3

    ; Clear pdpt 
    mov edi, boot_pdpt
    mov ecx, 1024
    xor eax, eax
    rep stosd

    ; Set up pdpt[0]..pdpt[3]
    mov edi, boot_pdpt
    mov dword [edi], boot_pd0 + 3
    mov dword [edi + 8], boot_pd0 + 4096 + 3
    mov dword [edi + 16], boot_pd0 + 4096 * 2 + 3
    mov dword [edi + 24], boot_pd0 + 4096 * 3 + 3

    ; clear pd0 
    mov edi, boot_pd0
    mov ecx, 4096
    xor eax, eax
    rep stosd

    ; clear pd0's PTs
    mov edi, boot_pd0_pts
    mov ecx, (1024 * (MAX_KERNEL_SIZE >> 21))
    xor eax, eax
    rep stosd

    ; add boot_pd0_pts to boot_pd0 
    mov ecx, (MAX_KERNEL_SIZE >> 21)
    mov edi, boot_pd0
    mov eax, boot_pd0_pts

    ;; First loop for setting up entries
.loop1:
    mov [edi], eax
    or dword [edi], 0x3
    add edi, 8
    add eax, 4096
    dec ecx
    jnz .loop1

    ;; Identity map the 0MB to MAX_KERNEL_SIZE range
    mov ecx, (512 * (MAX_KERNEL_SIZE >> 21))
    mov edi, boot_pd0_pts
    xor eax, eax

.loop2:
    mov [edi], eax
    or dword [edi], 0x3
    add edi, 8
    add eax, 4096
    dec ecx
    jnz .loop2

    ;; Map the rest with 2MiB pages
    mov ecx, (2048 - (MAX_KERNEL_SIZE >> 21))
    mov edi, boot_pd0 + (MAX_KERNEL_SIZE >> 21) * 8
    mov eax, MAX_KERNEL_SIZE 
    or eax, 0x83

.loop3:
    mov [edi], eax
    add edi, 8
    add eax, 1 << 21
    dec ecx
    jnz .loop3

    ;; Point CR3 to PML4T
    mov eax, boot_pml4t
    mov cr3, eax

    ;; Enable PAE + PSE
    mov eax, cr4
    or eax, 0x60
    mov cr4, eax

;; Enter Long-mode! ref(https://wiki.osdev.org/Setting_Up_Long_Mode)
.loop4:
    mov ecx, ENABLE_LM_MAGIC      ; Set the C-register to 0xC0000080, which is the EFER MSR.
    rdmsr                         ; Read from the model-specific register.
    or eax, 1 << ENABLE_LM_BIT    ; Set the LM-bit which is the 9th bit (bit 8).
    wrmsr                         ; Write to the model-specific register.

    ; enable PG
    mov eax, cr0
    or eax, 1 << ENABLE_PAGING_BIT
    mov cr0, eax

    pop ebx
    pop eax

    ; set up stack
    mov esp, stack_top
    and esp, -16

    push eax
    push ebx

    ; Now we are in 32-bit compatibility mode, We still need to load a 64-bit GDT
    mov eax, gdt64.pointer
    lgdt [eax]

    jmp code64_sel_value:_long_mode_start  ; Far jump to set CS

    hlt

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; JON-STR 03-05-2024
;;;     - 64 bit kernel
;;;         - boilerplate
;;;     - long mode start
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


global _long_mode_start
global reload_cr3

extern k_init

extern gdt64.data_seg

section .text
bits 64

_long_mode_start:
    cli

    pop rbx

    mov ecx, ebx ; Move the Multiboot address to ECX

    shr rbx, 32 ; Move the magic value to RBX

    ; using c args now
    ; mov qword [multiboot_info_ptr], rcx

    ; load null into all data segment registers
    mov ax, 0 ; gdt64.data_seg
    mov ss, ax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

; reload_cr3
    push rax
    mov rax, cr3
    mov cr3, rax
    pop rax

    mov rdi, rbx  ; Move the magic value to RDI, the first argument for k_init
    mov rsi, rcx  ; Move the Multiboot address to RSI, the second argument for k_init

    ; pop rdx       ; destroy stack value

    call k_init

.infinity:
    hlt
    jmp .infinity

reload_cr3:
    push rax
    mov rax, cr3
    mov cr3, rax
    pop rax
    ret
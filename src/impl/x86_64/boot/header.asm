;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; JON-STR 03-05-2024
;;;     - 64 bit kernel
;;;         - boilerplate
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MULTIBOOT2_MAGIC        equ 0xE85250D6
MULTIBOOT2_ARCH         equ 0x0
MULTIBOOT2_LENGTH       equ (header_end - header_start)
MULTIBOOT2_CHECKSUM     equ (0x100000000 - (MULTIBOOT2_MAGIC + MULTIBOOT2_ARCH + MULTIBOOT2_LENGTH))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
section .multiboot_header

align 0x8

header_start:
    ; magic number
    dd MULTIBOOT2_MAGIC

    ; architecture
    dd MULTIBOOT2_ARCH ; protected mode i386

    ; header length
    dd MULTIBOOT2_LENGTH

    ; checksum
    dd MULTIBOOT2_CHECKSUM
    
    ; end tag
    dw 0
    dw 0
    dd 8

header_end:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
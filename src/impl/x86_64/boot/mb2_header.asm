;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; JON-STR 03-05-2024
;;;     - 64 bit kernel
;;;         - boilerplate
;;;     - multiboot2 header
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MULTIBOOT2_MAGIC                equ 0xE85250D6
MULTIBOOT2_ARCH                 equ 0
MULTIBOOT2_LENGTH               equ (header_end - header_start)
MULTIBOOT2_CHECKSUM             equ (0x100000000 - (MULTIBOOT2_MAGIC + MULTIBOOT2_ARCH + MULTIBOOT2_LENGTH))

MULTIBOOT2_TAG_TYPE_FRAMEBUFFER equ     5
MULTIBOOT2_FRAMEBUFFER_WIDTH    equ     800
MULTIBOOT2_FRAMEBUFFER_HEIGHT   equ     600
MULTIBOOT2_FRAMEBUFFER_BPP      equ     32  ; Bits per pixel
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
section .multiboot2_header

align 8

header_start:
    dd MULTIBOOT2_MAGIC
    dd MULTIBOOT2_ARCH ; protected mode i386
    dd MULTIBOOT2_LENGTH
    dd MULTIBOOT2_CHECKSUM

    ; tags ????
    
    ; end tag
    dw 0
    dw 0
    dd 8

header_end:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
#include "../intf/debug_console.h"

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>

static inline void outportb(uint16_t port, uint8_t value)
{
    __asm__ volatile("outb %0, %1" : : "a"(value), "Nd"(port));
}

// later to be defined by gcc flags
// gcc -D__DEBUG_CONSOLE_ENABLED__=1 -o output_file source_file.c
#define __DEBUG_CONSOLE_ENABLED__ 1

#define QEMU_BOCHS_DEBUG_SERIAL_PORT 0xE9

void debug_console_putc(char c)
{
#ifdef __DEBUG_CONSOLE_ENABLED__
#if __DEBUG_CONSOLE_ENABLED__ == 1

    outportb(QEMU_BOCHS_DEBUG_SERIAL_PORT, c);

#endif
#endif
}

void debug_console_puts(const char *s)
{
    while (*s)
    {
        debug_console_putc(*s++);
    }
}

int strlen(const char *s)
{
    int len = 0;
    while (s[len])
    {
        ++len;
    }
    return len;
}

void reverse(char s[])
{
    int i, j;
    char c;

    for (i = 0, j = strlen(s) - 1; i < j; i++, j--)
    {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

void itoa(int n, char s[], int base)
{
    int i = 0, sign = n;
    bool isNegative = false;

    if (sign < 0)
    {
        isNegative = true;
        n = -n;
    }

    do {
        int digit = n % base;
        s[i++] = (digit > 9) ? (digit - 10) + 'a' : digit + '0';
    } while ((n /= base) > 0);

    if (isNegative)
    {
        s[i++] = '-';
    }

    s[i] = '\0';
    reverse(s);
}

void utoa(unsigned int n, char s[], int base)
{
    int i = 0;

    do {
        int digit = n % base;
        s[i++] = (digit > 9) ? (digit - 10) + 'a' : digit + '0';
    } while ((n /= base) > 0);

    s[i] = '\0';
    reverse(s);
}

void format_and_print(const char *format, va_list args) {
    for (int i = 0; format[i] != '\0'; i++) {
        if (format[i] == '%' && format[i + 1] != '%') {
            i++;  // Consume the '%'
            // Handle zero padding and width
            int padding = 0;
            bool zero_padding = format[i] == '0';
            if (zero_padding) {
                i++;  // Move past '0'
            }
            while (format[i] >= '0' && format[i] <= '9') {
                padding = padding * 10 + (format[i] - '0');
                i++;
            }

            char buffer[64];  // Ensure this buffer is large enough for any value.
            switch (format[i]) {
            case 's': {
                char *str = va_arg(args, char *);
                debug_console_puts(str);
                break;
            }
            case 'd': {
                int num = va_arg(args, int);
                itoa(num, buffer, 10);
                int buf_len = strlen(buffer);
                if (zero_padding && buf_len < padding) {
                    for (int p = 0; p < padding - buf_len; p++) {
                        debug_console_putc('0');
                    }
                }
                debug_console_puts(buffer);
                break;
            }
            case 'u': {
                unsigned int num = va_arg(args, unsigned int);
                utoa(num, buffer, 10);
                int buf_len = strlen(buffer);
                if (zero_padding && buf_len < padding) {
                    for (int p = 0; p < padding - buf_len; p++) {
                        debug_console_putc('0');
                    }
                }
                debug_console_puts(buffer);
                break;
            }
            case 'x':
            case 'X': {
                unsigned int num = va_arg(args, unsigned int);
                utoa(num, buffer, 16);
                // Convert to uppercase if needed
                if (format[i] == 'X') {
                    for (int j = 0; buffer[j]; j++) {
                        if (buffer[j] >= 'a' && buffer[j] <= 'f') {
                            buffer[j] -= 32;  // Convert to uppercase
                        }
                    }
                }
                int buf_len = strlen(buffer);
                if (zero_padding && buf_len < padding) {
                    for (int p = 0; p < padding - buf_len; p++) {
                        debug_console_putc('0');
                    }
                }
                debug_console_puts(buffer);
                break;
            }
            // Add more specifiers as needed.
            default:
                debug_console_putc(format[i]);  // Unrecognized format specifier, print as-is.
                break;
            }
        } else if (format[i] == '%' && format[i + 1] == '%') {
            debug_console_putc('%');
            i++;  // Skip next '%'
        } else {
            debug_console_putc(format[i]);
        }
    }
}


void debug_console_printf(const char *format, ...)
{
    va_list args;
    va_start(args, format);
    format_and_print(format, args);
    va_end(args);
}
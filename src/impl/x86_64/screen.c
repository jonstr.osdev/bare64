#include "../intf/screen.h"

#include <stdint.h>
#include <stddef.h>

typedef struct {
    uint8_t character;
    uint8_t color;
} scn_char_t;

#define SCREEN_WIDTH 80
#define SCREEN_HEIGHT 25

#define SCREEN_BUFFER_ADDR 0xB8000

scn_char_t *scn_buffer = (scn_char_t *) SCREEN_BUFFER_ADDR;
size_t scn_row = 0;
size_t scn_col = 0;
uint8_t scn_color = (SCREEN_COLOR_BLACK << 4) | (SCREEN_COLOR_WHITE & 0x0F);

void scn_clear_row(size_t row)
{
    scn_char_t empty = {
        .character = ' ',
        .color = scn_color
    };

    for (size_t col = 0; col < SCREEN_WIDTH; col++)
    {
        scn_buffer[col + row * SCREEN_WIDTH] = empty;
    }
}


void scn_newline()
{
    scn_col = 0;

    if (scn_row < SCREEN_HEIGHT - 1)
    {
        scn_row++;
        return;
    }

    for(size_t row = 1; row < SCREEN_HEIGHT; row++)
    {
        for(size_t col = 0; col < SCREEN_WIDTH; col++)
        {
            scn_buffer[col + (row - 1) * SCREEN_WIDTH] = scn_buffer[col + row * SCREEN_WIDTH];
        }
    }

    scn_clear_row(SCREEN_HEIGHT - 1);
}


void screen_cls()
{
    for(size_t row = 0; row < SCREEN_HEIGHT; row++)
    {
        scn_clear_row(row);
    }
}


void screen_putc(const char c)
{
    scn_char_t character = {
        .character = c,
        .color = scn_color
    };

    if (c == '\n')
    {
        scn_col = 0;
        scn_row++;
    }
    else
    {
        scn_buffer[scn_col + scn_row * SCREEN_WIDTH] = character;
        scn_col++;
    }

    if (scn_col >= SCREEN_WIDTH)
    {
        scn_col = 0;
        scn_row++;
    }

    if (scn_row >= SCREEN_HEIGHT)
    {
        scn_row = 0;
    }
}


void screen_puts(const char *s)
{
    for (size_t i = 0; s[i] != '\0'; i++)
    {
        screen_putc(s[i]);
    }
}


void screen_set_color(uint8_t fg, uint8_t bg)
{
    scn_color = (bg << 4) | (fg & 0x0F);
}
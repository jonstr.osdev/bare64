#pragma once

#include <stdarg.h>

void debug_console_printf(const char *format, ...);
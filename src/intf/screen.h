#pragma once

#include <stdint.h>

enum
{
    SCREEN_COLOR_BLACK          = 0,
    SCREEN_COLOR_BLUE           = 1,
    SCREEN_COLOR_GREEN          = 2,
    SCREEN_COLOR_CYAN           = 3,
    SCREEN_COLOR_RED            = 4,
    SCREEN_COLOR_MAGENTA        = 5,
    SCREEN_COLOR_BROWN          = 6,
     SCREEN_COLOR_LIGHT_GRAY    = 7,
    SCREEN_COLOR_DARK_GRAY      = 8,
    SCREEN_COLOR_LIGHT_BLUE     = 9,
    SCREEN_COLOR_LIGHT_GREEN    = 0x0A,
    SCREEN_COLOR_LIGHT_CYAN     = 0x0B,
    SCREEN_COLOR_LIGHT_RED      = 0x0C,
    SCREEN_COLOR_PINK           = 0x0D,
    SCREEN_COLOR_YELLOW         = 0x0E,
    SCREEN_COLOR_WHITE          = 0x0F
};

void screen_cls();
void screen_putc(const char c);
void screen_puts(const char *s);
void screen_set_color(uint8_t fg, uint8_t bg);